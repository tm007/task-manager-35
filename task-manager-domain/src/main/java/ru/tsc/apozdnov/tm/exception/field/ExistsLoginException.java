package ru.tsc.apozdnov.tm.exception.field;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class ExistsLoginException extends AbstractException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}